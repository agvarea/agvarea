# Test App (agvarea) #
The application is running on: https://agvarea.bitbucket.io

## Getting Started

To get you started you can simply clone the agvarea repository and install the dependencies:

### Prerequisites

[git](http://git-scm.com/)

[node](http://nodejs.org/).

[npm](http://npmjs.com/).


### Clone agvarea repository

Clone porject
```sh
   git clone https://agvarea@bitbucket.org/agvarea/agvarea.git
```

Move to folder of project
```sh
   cd agvarea/
```
### Install dependecies

* We get the dependencies via `npm`

```sh
   npm install
```

You should find a new folder in project folder.

* `node_modules` - contains the npm packages 

### Run the Application

```
npm start
```

Now browse to the app at `http://localhost:3000/`.

### Run build application

```
npm run buil
```

After run build we will find a new forlder called `build`

### Npm scripts

`start`      --> Run Server 

`build-css`  --> Compiles SASS files into a main CSS file

`build`      --> Build the application

### Dependencies

`react-create-app` Library used to get the initial configuration of a React app. The library provides a basic configuration of webpack and React application.

`axios` Library used to do request to the API.

`lodash` Library used to manage colections. 

`react` Library to build user interface. I used React because is a very powerful library with a lot of benefits like modularization and its easy write components. React has 

`react-slick` Library to create an images carousel. I used it because provides a lot of methods to customize it. For example the `lazyLoad` property and infinite slider

`redux` I used redux to centralize the application state. In this case we have a very small state but is better setup redux from the beginning to scale properly

`redux-promise` I setup this middleware to dispatch async requests from redux actions. You can dispatch an async request from the action and the reducer gets the response into the payload.
