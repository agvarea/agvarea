import React, { Component } from 'react';
import {
  Route,
  HashRouter,
  Switch
} from 'react-router-dom';
import { Provider } from 'react-redux';

import configureStore from '../store';
import routes from '../routes';

const store = configureStore();

class App extends Component {
  render() {
    return (
      <div className="main-layout">
        <Provider store={store}>
          <HashRouter>
            <Switch>
              {
                routes.map((prop, key) => {
                  return (
                    <Route path={prop.path} key={key} component={(props) => <prop.component />} />
                  )
                })
              }
            </Switch>
          </HashRouter>
        </Provider>
      </div>
    );
  }
}

export default App;
