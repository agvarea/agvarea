import Item from '../components/item/Item';

const routes = [
  { path: '/', name: 'Item', component: Item }
]

export default routes;