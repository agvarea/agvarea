import { createStore, applyMiddleware, compose } from 'redux';
import promiseMiddleware from 'redux-promise';
import rootReducers from '../reducers';

const createMiddlewares = () => {
  let middleware = [promiseMiddleware]
  if (process.env.NODE_ENV !== 'production' && window.__REDUX_DEVTOOLS_EXTENSION__) {
    const dev_tool = window.__REDUX_DEVTOOLS_EXTENSION__()
    return compose(
      applyMiddleware(...middleware),
      dev_tool
    )
  }
  return applyMiddleware(...middleware)
}

const configureStore = (initialState) => {
  const store = createStore(
    rootReducers,
    initialState,
    createMiddlewares(),
  )
  return store;
}

export default configureStore;