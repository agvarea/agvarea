import { GET_ITEM } from '../constants/action.constants';

const INITIAL_STATE = {
  product: { }
}

export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case GET_ITEM:
      return Object.assign({}, state, {
        product: payload.data.product
      });
    default:
      return state;
  }
}