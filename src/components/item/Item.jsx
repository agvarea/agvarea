import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Slider from 'react-slick';
import lodash from 'lodash';

import { getItem } from '../../actions/item.actions';
const BAND_SIZE = 2;
const DEFAULT_ITEM_INDEX = 0;

class Item extends Component {
  constructor(props) {
    super(props);
    this.state = {
      color: '',
      availableSizes: [],
      cupSizeOptions: [],
      selectedImage: '',
      variantsKey: [
        'naked-1', 
        'naked-2', 
        'naked-3', 
        'naked-4', 
        'naked-5'
      ]
    }
  }

  _getBandSize = (option) => {
    return option.substring(0, BAND_SIZE);
  }

  _getCupSize = (option) => {
    return option.substring(BAND_SIZE);
  }

  _groupElementsBy = (variants, keyGetter) => {
    const variantsInfo = new Map();
    variants.forEach((variant) => {
      const key = keyGetter(variant);
      const collection = variantsInfo.get(key);
      if (!collection) {
        variantsInfo.set(key, [variant]);
      } else {
        collection.push(variant);
      }
    });
    this.setState({ variantsInfo }, () => this._selectVariant());
  }

  _selectVariant = (variant = this.state.variantsKey[DEFAULT_ITEM_INDEX], index = DEFAULT_ITEM_INDEX) => {
    let selectedVariantColor = this.state.variantsInfo.get(variant);
    let availableSizes = selectedVariantColor.map(option => this._getBandSize(option.option2));
    availableSizes = lodash.uniq(availableSizes);
    let selectedImage = this.props.product.images[DEFAULT_ITEM_INDEX].srcMaster
    let variantOption = selectedVariantColor[index];
    let price = variantOption.price;
    let color = variantOption.option1;
    let stock = variantOption.inventory_quantity;
    this.setState({ 
      price,
      color,
      stock,
      selectedImage,
      availableSizes,
      selectedVariantColor,
    }, () => this._handleBandSize(variantOption.option2));
  }

  _handleChange = (event) => {
    const target = event.target;
    let name = target.name;
    switch(name) {
      case 'band_size': {
        this._handleBandSize(target.value);
        break;
      }
      case 'cup_size': {
        this._handleCupSize(target.value);
        break;
      }
      default: {
        break;
      }
    }
  }

  _handleBandSize = (bandSize) => {
    let cupSizeOptions = this.state.selectedVariantColor.filter((variant, index) => {
      return variant.option2.indexOf(bandSize) > -1
    });
    let { option2, inventory_quantity, price } = cupSizeOptions[0];
    let cupSize = this._getCupSize(option2);
    let stock = inventory_quantity;
    this.setState({ stock, price, bandSize, cupSize, cupSizeOptions });
  }

  _handleCupSize = (cupSize) => {
    let variantSelected = this.state.selectedVariantColor.find(variant => {
      return variant.option2 === `${this.state.bandSize}${cupSize}`
    })
    let stock = variantSelected.inventory_quantity;
    let price = variantSelected.price;
    this.setState({ stock, price, cupSize });
  }

  _addToCart = () => {
    alert(`Added a ${this.props.product.title} - ${this.state.bandSize + this.state.cupSize} to the cart`);
  }

  _selectImage = (selectedImage) => {
    this.setState({ selectedImage })
  }

  updateWindowDimensions = () => {
    this.setState({ windowWidth: window.innerWidth });
  }

  componentWillUnmount = () => {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.props.product !== nextProps.product) {
      this._groupElementsBy(nextProps.product.variants, variant => variant.option1);
    }
  }

  componentDidMount = () => {
    this.props.getItem();
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }

  render() {
    const sliderSettings = {
      dots: true,
      lazyLoad: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    
    if (this.props.product.title) {
      return(
        <div className="product">
          <div className="product__header__container desktop">
            <h1 className="product__title">{this.props.product.title}</h1>
            <p className="product__price">${Math.round(this.state.price)}</p>
          </div>

          <div className="product__image">
            {
              this.state.windowWidth > 768 ? 
                <div className="product__image__container">
                  <div className="product__image__carousel">
                      {
                        this.props.product.images.map((image, key) => {
                          return(
                            <div onClick={() => this._selectImage(image.srcMaster)} key={key}>
                              <img className="product__image__thumb" 
                                  src={image.src150} alt=""/>
                            </div>
                          )
                        })
                      }
                  </div> 
                  <img className="product__image__preview" 
                    src={this.state.selectedImage} alt="" />
                </div> 
                :
                <div className="product__image__slider">
                  <Slider {...sliderSettings}>
                    {
                      this.props.product.images.map((image, key) => {
                        return(
                          <img src={image.srcMaster} key={key} alt=''/>
                        )
                      })
                    }
                  </Slider>
                </div>
            }
          </div>

          <div className="product__selection__container">
            <div className="product__header__container mobile">
              <h1 className="product__title">{this.props.product.title}</h1>
              <p className="product__price">${Math.round(this.state.price)}</p>
            </div>

            <div className="product__selection">
              <div className="product__color">
                <p className="product__color__title">color: <b>{this.state.color.replace('-', ' ')}</b></p>
                <div className="product__color__picker">
                  {
                    this.state.variantsKey.map((variant, key) => {
                      return (
                        <span onClick={() => this._selectVariant(variant)} key={key} 
                          className={"product__color__picker__dot " + variant}></span>
                      )
                    })
                  }
                </div>
              </div>

              <div className="product__stock">
                <p className="product__stock__title">stock: <b>{this.state.stock}</b></p>
              </div>

              <div className="product__size">
                <div className="product__size__select">
                  <p className="product__size__select__title">band size</p>
                  <select onChange={this._handleChange} 
                    value={this.state.bandSize}
                    name='band_size'>
                    {
                      this.state.availableSizes.map((size, key) => {
                        return(
                          <option value={size} key={key}>
                            { size }
                          </option>   
                        )
                      })
                    }
                  </select>
                </div>
                <div className="product__size__select">
                  <p className="product__size__select__title">cup size</p>
                  <select onChange={this._handleChange}
                    value={this.state.cupSize}
                    name='cup_size'>
                    {
                      this.state.cupSizeOptions.map((variant, key) => {
                        return(
                          <option value={this._getCupSize(variant.option2)} key={key}>
                            { this._getCupSize(variant.option2) }
                          </option>
                        )
                      })
                    }
                  </select>
                </div>
              </div>

              <div className="product__add">
                <button onClick={this._addToCart} className="product__add_button"> add to bag</button>
              </div>
            </div>
          </div>

          <div className="product__detail">
            <p className="product__detail__title">details</p>
            <hr className="product__detail__separator"/>
            <div dangerouslySetInnerHTML={{ __html: this.props.product.body_html }} />
          </div>
        </div>   
      )
    } else {
      return('')
    }
  }
}

const mapStateToProps = (state) => {
  return state.item; 
}

const mapDispatchToProps = (dispatch) => {
  return {
    getItem: bindActionCreators(getItem, dispatch),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Item);