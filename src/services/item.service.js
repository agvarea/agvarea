import axios from 'axios'
import settings from '../config/url.settings'

const BASE_URL = settings.items_url;

class ItemService {
  getItem = () => {
    return axios.get(BASE_URL)
  }
}

export default ItemService
