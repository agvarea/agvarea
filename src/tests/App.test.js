import React from 'react';
import renderer from 'react-test-renderer';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import App from '../views/App';
import Item from '../components/item/Item';

Enzyme.configure({ adapter: new Adapter() });

test('renders without crashing', () => {
  const component = renderer.create(
    <App/>
  );

  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});