import React from 'react';
import renderer from 'react-test-renderer';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import configureStore from 'redux-mock-store'

import Item from '../components/item/Item';

Enzyme.configure({ adapter: new Adapter() });

const mockStore = configureStore()
const initialState = { item: { name: 'Item Test' } }

test('item component is rendered correctly', () => {
  let store = mockStore(initialState)
  const item = shallow(<Item store={store} />)
  expect(item.length).toEqual(1)
});

test('check if item component props matches with initialState', () => {
  let store = mockStore(initialState)
  const item = shallow(<Item store={store} />)
  expect(item.prop('item.name')).toEqual(initialState.name)
});