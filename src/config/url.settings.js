const settings = {
    host_url: 'https://www.mocky.io/v2/',
    get items_url() { return this.host_url + '5b3a92482e0000eb20158265' }
}

export default settings;