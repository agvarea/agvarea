import ItemService from '../services/item.service';
import { GET_ITEM } from '../constants/action.constants';

export const getItem = () => {
  let itemService = new ItemService();
  const request = itemService.getItem()
  return {
    type: GET_ITEM,
    payload: request
  }
}